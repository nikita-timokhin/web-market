import {Component, OnInit} from '@angular/core';
import {ProductAtBucket} from "./productAtBucket";
import {Product} from "../market/product";
import {BucketService} from "./bucketService";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ProductService} from "../market/product.service";
import {User} from "../registration/user";
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {HistoryOrder} from "./HistoryOrder";

@Component({
  selector: 'app-bucket',
  templateUrl: './bucket.component.html',
  styleUrls: ['./bucket.component.less']
})
export class BucketComponent implements OnInit {
  productB: ProductAtBucket[] = [];
  user: User | any;
  rating: number= 0;
  historyOrder: HistoryOrder[] = [];
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  displayMin: boolean = false;
  displayMaximizable: boolean = false;
  displayHistory: boolean = false;
  displayRating: boolean = false;

  constructor(public bucketService: BucketService, public jwtHelper: JwtHelperService, public productService: ProductService, private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.productB = this.bucketService.returnProduct();
    // @ts-ignore
    this.discountAmount = this.jwtHelper.decodeToken(localStorage.getItem("accessToken")).amount
    this.user = this.productService.user;
    this.historyOrder = this.bucketService.returnProduct();
    for(let p of this.historyOrder){
      p.userId = this.user.id;
    }
  }

  openSnackBar() {
    this._snackBar.open("Done", "Ok", {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });

  }

  deleteBucket() {
    this.productB = this.bucketService.deletedBucket();
  }


  changeAmount(amount: number) {
    this.user.amount += amount;
  }

  showMaximizableDialog() {
    this.displayMaximizable = true;
  }
  showHistory() {
    this.displayHistory = true;
  }

  showMin() {
    this.displayMin = true;
  }

  delete(product: Product[]) {
    console.log("whaaaaaaaat?")
    this.bucketService.deleteProduct(product)
      .subscribe(
        (data: any) => {
          console.log("ok")
        },
        (error: any) => console.log("error")
      );this.historySet(this.productB);
  }

  increase(user: User) {
    this.bucketService.increaseAmount(user)
      .subscribe(
        (data: any) => {
          console.log("ok")
        },
        (error: any) => console.log("error")
      );
  }

  ClickConfirm(){
    this.displayMaximizable=false;
    this.displayMin=true;
    this.changeAmount(this.bucketService.amount);
  }

  onClick(){
    this.openSnackBar();
    this.displayMin=false;
    this.delete(this.productB);
    this.increase(this.user);
    this.deleteBucket();
  }

  historySet(productB: ProductAtBucket[]) {
    this.bucketService.setHistory(productB)
      .subscribe(
        (data: any) => {
          console.log("ok")
        },
        (erorr: any) => console.log("error")
      );
  }

  History(user: User) {
    this.bucketService.getHistory(user).subscribe(data => {
      this.historyOrder = data;
    });
    this.showHistory();
  }
}
