import {Product} from "../market/product";

export class HistoryOrder extends Product {
  id?: string;
  name?: string;
  price?: number;
  image?: string;
  rate?: number;
  stock?: number;
  userId?: number;
}
