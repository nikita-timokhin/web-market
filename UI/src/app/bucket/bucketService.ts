import {Injectable} from '@angular/core';
import {Product} from "../market/product";
import {ProductAtBucket} from "./productAtBucket";
import {HttpClient} from '@angular/common/http';
import {User} from "../registration/user";
import {environment} from "../../environments/environment";


@Injectable({providedIn: "root"})
export class BucketService {
  products: Product[] = [];
  amount: number = 0;

  constructor(private http: HttpClient) {
  }

  setProducts(product: Product) {
    this.products.push(product);
  }

  setHistory(products: Product[]){
    return this.http.post<any>(environment.apiUrl + '/history/set', products,{headers:{
        "Authorization": "Bearer " + localStorage.getItem("accessToken")}})
  }
  getHistory(user: User){
    return this.http.post<any>(environment.apiUrl + '/history/list', user, {headers:{
        "Authorization": "Bearer " + localStorage.getItem("accessToken")}})
  }

  deletedBucket(){
    this.products = []
    return this.products
  }

  returnProduct() {
    return this.products;
  }

  order(productB: ProductAtBucket[], discountAm: number) {
    let i: any;
    let sum: number = 0;
    for (i of productB) {
      sum +=i.price - i.price * this.discount(discountAm);
      this.amount = sum;
    }
    return sum;
  }

  increaseAmount(user: User){
    return this.http.post<any>(environment.apiUrl + '/user/alter', user,{headers:{
        "Authorization": "Bearer " + localStorage.getItem("accessToken")}})
  }

  discount(discountAm: number){
    let disc: number = 0;
    if (discountAm >= 10000 && discountAm < 20000){
      disc = 0.1;
    }else if (discountAm >= 20000){
      disc = 0.2;
    }else disc = 0;
    return disc;
  }

  deleteProduct(productB: ProductAtBucket[]) {
    let i: any;
    for (i of productB) {
     i.stock-=1;
    }return this.http.post(environment.apiUrl + '/prod/reduce', productB, {headers:{
        "Authorisation": "Bearer " + localStorage.getItem("accessToken")
      }});
  }

}
