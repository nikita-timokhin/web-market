import {Component, OnInit} from '@angular/core';
import {ProductService} from "./product.service";
import {Product} from './product';
import {SelectItem} from 'primeng/api';
import {PrimeNGConfig} from 'primeng/api';
import {BucketService} from "../bucket/bucketService";
import {JwtHelperService} from '@auth0/angular-jwt';
import {Authentication} from "../authentication/authentication";
import {AuthenticationService} from "../authentication/authentication.service";
import {User} from "../registration/user";

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.less']
})
export class MarketComponent implements OnInit {
  products: Product[] = [];
  isAdmin: boolean = false;
  isMod: boolean = false;
  sortOptions: SelectItem[] = [];
  user = new User("", "", 0, 0, new Authentication("",""))
  product: Product = new Product();
  sortOrder: number = 0;
  currentProduct: Product = {};
  sortField: string = "";
  sortKey: any;

  constructor(public productService: ProductService, private primengConfig: PrimeNGConfig, public bucketService: BucketService, public jwtHelper: JwtHelperService, public authenticationService: AuthenticationService) {
  }


  ngOnInit(): void {
    this.productService.getProducts().subscribe(data => {
      this.products = data;
    });
    this.user = this.productService.user

    // @ts-ignore
    this.isAdmin = this.jwtHelper.decodeToken(localStorage.getItem("accessToken")).type === "moderator" || this.jwtHelper.decodeToken(localStorage.getItem("accessToken")).type === "admin";
    // @ts-ignore
    this.isMod = this.jwtHelper.decodeToken(localStorage.getItem("accessToken")).type === "moderator"
    this.sortOptions = [
      {label: 'Price High to Low', value: '!price'},
      {label: 'Price Low to High', value: 'price'},
      {label: 'Rate High to Low', value: '!rate'},
      {label: 'Rate Low to High', value: 'rate'},
    ];

    this.primengConfig.ripple = true;
  }

  addProducts(newProduct: Product) {
    this.productService.addProduct(newProduct)
      .subscribe(
        (data: any) => {
          console.log("ok")
          this.products = [...this.products, data];
        },
        (error: any) => console.log("error")
      );
  }

  displayAdd = false;

  showAdd() {
    this.displayAdd = true;
  }

  displayDel = false;

  showDel() {
    this.displayDel = true;
  }

  preAlter(productToD: Product) {
    this.currentProduct = productToD
  }

  alter(){
    this.productService.alterProduct(this.currentProduct)
      .subscribe(
        (data: any) => {
          console.log("ok")
          this.products = [...this.products];
        },
        (error: any) => console.log("error")
      );
  }

  delete() {
    this.productService.delProduct(this.currentProduct)
      .subscribe(
        (data: any) => {
          console.log("ok")
          this.products = this.products.filter(product => product.id !== this.currentProduct.id)
        },
        (error: any) => console.log("error")
      );
  }

  onSortChange(event: any) {
    let value = event.value;
    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }
}
