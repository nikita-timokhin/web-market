export class Product {
  id?:string;
  name?:string;
  price?:number;
  image?:string;
  rate?:number;
  stock?:number;
}
