import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Product} from "./product";
import {Authentication} from "../authentication/authentication";
import {User} from "../registration/user";
import {environment} from "../../environments/environment";

@Injectable({providedIn: "root"})
export class ProductService {
  user: User | any;

  constructor(private http: HttpClient) {
  }

  getProducts() {
    return this.http.get<any>(environment.apiUrl + '/prod/list', {headers:{
        "Authorization": "Bearer " + localStorage.getItem("accessToken")
      }})
  }
  addProduct(product: Product){
    return this.http.post<any>(environment.apiUrl + '/prod/add', product,{headers:{
        "Authorization": "Bearer " + localStorage.getItem("accessToken")}})
  }
  delProduct(product: Product){
    return this.http.post<any>(environment.apiUrl + '/prod/del', product,{headers:{
        "Authorization": "Bearer " + localStorage.getItem("accessToken")}})
  }
  alterProduct(product: Product){
    return this.http.post<any>(environment.apiUrl + '/prod/alter', product,{headers:{
        "Authorization": "Bearer " + localStorage.getItem("accessToken")}})
  }
  setUser(authentication: Authentication){
    return this.http.post(environment.apiUrl + '/auth/user', authentication, {headers:{
        "Authorisation": "Bearer " + localStorage.getItem("accessToken")
      }});
  }
  setUsers(authentication: Authentication) {
    this.setUser(authentication)
      .subscribe(
        (data: any) => {
          this.user = data
        }, error => {
          console.log("error");
        }
      );
  }
}
