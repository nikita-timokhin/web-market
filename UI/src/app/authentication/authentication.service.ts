import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Authentication} from "./authentication";
import { environment } from '../../environments/environment';

@Injectable({providedIn: "root"})
export class AuthenticationService {


  constructor(private http: HttpClient){ }

  validateAuth(authentication: Authentication){
    return this.http.post(environment.apiUrl + '/auth/verify', authentication, {headers:{
      "Authorisation": "Bearer " + localStorage.getItem("accessToken")
      }});
  }
}
