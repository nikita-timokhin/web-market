import {Component, OnInit} from '@angular/core';
import {Authentication} from "./authentication";
import {AuthenticationService} from "./authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../registration/user";
import {ProductService} from "../market/product.service";


@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.less']
})
export class AuthenticationComponent implements OnInit {
  authentication = new Authentication("", "");
  user = new User("", "", 0, 0, this.authentication)


  constructor(public authenticationService: AuthenticationService, private route: ActivatedRoute,
              private router: Router,
              public productService: ProductService) {
  }

  submitAuth(authentication: Authentication) {
    this.authenticationService.validateAuth(authentication)
      .subscribe(
        (data: any) => {
          localStorage.setItem("accessToken", data.accessToken);
          this.router.navigateByUrl('market');
        }, error => {
          console.log("error verification");
        }
      );
  }


  ngOnInit(): void {

  }
}
