import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthenticationComponent} from "./authentication/authentication.component";
import {RegistrationComponent} from "./registration/registration.component";
import {MarketComponent} from "./market/market.component";
import {BucketComponent} from "./bucket/bucket.component";

const routes: Routes = [
  {path: '' , redirectTo:'/auth',pathMatch:'full'},
  {path: 'auth', component: AuthenticationComponent},
  {path: 'auth/reg', component: RegistrationComponent, pathMatch: 'full'},
  {path: 'market', component: MarketComponent, pathMatch: 'full'},
  {path: 'market/bucket', component: BucketComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
