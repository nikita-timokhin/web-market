export class User{
  constructor(public name:string, public email:string, public phone:number, public amount:number, public authentication: Authentication){}
}
export class Authentication{
  constructor(public login:string, public pass:string) {
  }
}
