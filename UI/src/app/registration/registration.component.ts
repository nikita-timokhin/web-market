import {Component, OnInit } from '@angular/core';
import {HttpUserService} from "./http.user.service";
import {Authentication, User} from './user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})
export class RegistrationComponent implements OnInit {user: User=new User("", "",0,0,new Authentication("",""));

  done: boolean = false;
  constructor(private httpService: HttpUserService){}
  submit(user: User){
    this.httpService.postData(user)
      .subscribe(
        (data: any) => {console.log("ok")},
        (error: any) => console.log("error registration")
      );
  }

  ngOnInit(): void {
  }

}

