import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user';
import {environment} from "../../environments/environment";

@Injectable({providedIn: "root"})
export class HttpUserService {

  constructor(private http: HttpClient){ }

  postData(user: User){
    return this.http.post(environment.apiUrl + '/auth/add', user);
  }
}
