FROM openjdk:8-alpine
COPY /target/entrypoint-spring-boot.jar /entrypoint/
ENTRYPOINT ["java","-jar","/entrypoint/entrypoint-spring-boot.jar"]

