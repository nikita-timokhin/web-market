/*
 * Copyright
 */

package webmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webmarket.models.OrderHistory;
import webmarket.models.Product;
import webmarket.repository.HistoryRepository;
import webmarket.repository.ProductRepository;

import java.util.List;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final HistoryRepository historyRepository;

    @Autowired
    public ProductService(ProductRepository productRepository, HistoryRepository historyRepository) {
        this.productRepository = productRepository;
        this.historyRepository = historyRepository;
    }

    public void reduceProduct(Product[] products) {
        for (Product p : products) {
            Product prod = productRepository.getByName(p.getName()).get();
            prod.setStock(p.getStock());
            List<OrderHistory> histories = historyRepository.findAllByName(p.getName());
            int histSum = 0;
            for (OrderHistory h : histories) {
                histSum += h.getRate();
            }
            if (p.getRate() > 5){
                p.setRate(5);
            }
            prod.setRate((histSum + p.getRate()) / (histories.size() + 1));
            productRepository.save(prod);
        }
    }

    public void deleteProduct(Product product) {
        productRepository.deleteById(productRepository.getByName(product.getName()).get().getId());
    }

    public void alterProduct(Product product) {
        Product prod = productRepository.findById(product.getId()).get();
        prod.setStock(product.getStock());
        prod.setName(product.getName());
        prod.setPrice(product.getPrice());
        prod.setRate(product.getRate());
        prod.setImage(product.getImage());
        productRepository.save(prod);
    }

}
