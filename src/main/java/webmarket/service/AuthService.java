/*
 * Copyright
 */

package webmarket.service;

import webmarket.models.Authentication;
import webmarket.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webmarket.repository.AuthenticationRepository;
import webmarket.repository.UserRepository;

import java.util.Optional;

@Service
public class AuthService {
    private UserRepository userRepository;
    private AuthenticationRepository authenticationRepository;


    @Autowired
    public AuthService(UserRepository userRepository, AuthenticationRepository authenticationRepository) {
        this.userRepository = userRepository;

        this.authenticationRepository = authenticationRepository;
    }

    public void register(User user) {
        this.userRepository.save(user);
    }

    public boolean verifyUser(Authentication authentication) {
        return authenticationRepository.getByLoginAndPass(authentication.getLogin(), authentication.getPass()).equals(Optional.empty());
    }

    public User returnUser(Authentication authentication){
        return authenticationRepository.getByLoginAndPass(authentication.getLogin(), authentication.getPass()).get().getUser();
    }


}
