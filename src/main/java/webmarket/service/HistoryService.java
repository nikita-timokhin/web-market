/*
 * Copyright
 */

package webmarket.service;

import org.springframework.stereotype.Service;
import webmarket.models.OrderHistory;
import webmarket.repository.HistoryRepository;


@Service
public class HistoryService {

    private final HistoryRepository historyRepository;

    public HistoryService(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    public void historySet(OrderHistory[] orderHistories) {
        for (OrderHistory h : orderHistories) {
            if(h.getRate() > 5){
                h.setRate(5);
            }
            this.historyRepository.save(new OrderHistory(h.getName(),h.getPrice(), h.getRate(),h.getStock(), h.getUserId()));
        }
    }

}
