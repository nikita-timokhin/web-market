/*
 * Copyright
 */

package webmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webmarket.models.User;
import webmarket.repository.UserRepository;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void increaseAmount(User user){
        User currentUser = userRepository.findById(user.getId()).get();
        currentUser.setAmount(user.getAmount());
        userRepository.save(currentUser);
    }
}
