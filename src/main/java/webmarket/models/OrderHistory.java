/*
 * Copyright
 */

package webmarket.models;

import javax.persistence.*;

@Entity
@Table(schema = "public", name = "history")
public class OrderHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private float price;
    @Column(name = "rate")
    private int rate;
    @Column(name = "stock")
    private int stock;
    @Column(name = "userId")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public OrderHistory() {
    }

    public OrderHistory(String name, float price, int rate, int stock, int userId) {
        this.name = name;
        this.price = price;
        this.rate = rate;
        this.stock = stock;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getPrice() {
        return price;
    }

    public int getRate() {
        return rate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
