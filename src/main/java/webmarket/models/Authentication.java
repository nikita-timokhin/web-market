package webmarket.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(schema = "public", name = "auth")
public class Authentication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;
    @Column(name = "login")
    private String login;
    @Column(name = "pass")
    private int pass;

    public User getUser() {
        return user;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public int getPass() {
        return pass;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPass(String pass) {
        this.pass = pass.hashCode();
    }


}
