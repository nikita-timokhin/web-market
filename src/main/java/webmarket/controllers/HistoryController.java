/*
 * Copyright
 */

package webmarket.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import webmarket.models.OrderHistory;
import webmarket.models.User;
import webmarket.repository.HistoryRepository;
import webmarket.service.HistoryService;

import java.util.List;


@RestController
@RequestMapping("history")
public class HistoryController {
    private final HistoryRepository historyRepository;
    private final HistoryService historyService;

    public HistoryController(HistoryRepository historyRepository, HistoryService historyService) {
        this.historyRepository = historyRepository;
        this.historyService = historyService;
    }

    @PostMapping("/list")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public List<OrderHistory> getHistory(@RequestBody User user) {
        return this.historyRepository.findAllByUserId(user.getId());
    }

    @PostMapping("/set")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void setHistory(@RequestBody OrderHistory[] orderHistory) {
        this.historyService.historySet(orderHistory);
    }

}
