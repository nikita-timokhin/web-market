package webmarket.controllers;

import org.springframework.http.HttpStatus;
import webmarket.models.Product;
import org.springframework.web.bind.annotation.*;
import webmarket.repository.HistoryRepository;
import webmarket.repository.ProductRepository;
import webmarket.service.ProductService;


@RestController
@RequestMapping("prod")
public class ProductController {
    private final ProductRepository productRepository;
    private final ProductService productService;
    private final HistoryRepository historyRepository;

    private ProductController(ProductRepository productRepository, ProductService productService, HistoryRepository historyRepository) {
        this.productRepository = productRepository;
        this.productService = productService;
        this.historyRepository = historyRepository;
    }

    @GetMapping("/list")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Product> allPosts() {
        return this.productRepository.findAll();
    }

    @PostMapping("/add")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public Product add(@RequestBody Product product) {
        return this.productRepository.save(product) ;
    }

    @PostMapping("/del")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void dell(@RequestBody Product product) {
        productService.deleteProduct(product);
    }

    @PostMapping("/reduce")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void reduce(@RequestBody Product[] product) {
        this.productService.reduceProduct(product);

    }@PostMapping("/alter")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void alter(@RequestBody Product product) {
        this.productService.alterProduct(product);
    }
}
