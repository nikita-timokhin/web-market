package webmarket.controllers;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import webmarket.AuthResponse;
import webmarket.service.AuthService;
import webmarket.models.Authentication;


import webmarket.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import webmarket.repository.AuthenticationRepository;

import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Date;


@RestController
@RequestMapping("auth")
public class AuthenticationController {
    private final AuthService authService;
    private final AuthenticationRepository authenticationRepository;

    @Autowired
    public AuthenticationController(AuthenticationRepository authenticationRepository, AuthService authService, HttpServletResponse response) {
        this.authenticationRepository = authenticationRepository;
        this.authService = authService;
    }

    @GetMapping("/list")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Authentication> allPosts() {
        return this.authenticationRepository.findAll();
    }

    @PostMapping("/user")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public User takeUser(@RequestBody Authentication authentication){
        return authService.returnUser(authentication);
    }

    @PostMapping("/add")
    @CrossOrigin
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public void add(@RequestBody User user) {
        user.getAuthentication().setUser(user);
        this.authService.register(user);
    }

    @PostMapping("/del")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void delete() {

        this.authenticationRepository.deleteAll();
    }


    @PostMapping("/verify")
    @CrossOrigin
    public ResponseEntity<AuthResponse> verify(@RequestBody Authentication authentication) {
        if (authService.verifyUser(authentication)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        String jws = Jwts.builder()
            .claim("login", authentication.getLogin())
            .claim("type", authenticationRepository.getByLoginAndPass(authentication.getLogin(),authentication.getPass()).get().getUser().getType())
            .claim("amount", authenticationRepository.getByLoginAndPass(authentication.getLogin(),authentication.getPass()).get().getUser().getAmount())
            // Fri Jun 24 2016 15:33:42 GMT-0400 (EDT)
            .setIssuedAt(Date.from(Instant.ofEpochSecond(1466796822L)))
            // Sat Jun 24 2116 15:33:42 GMT-0400 (EDT)
            .setExpiration(Date.from(Instant.ofEpochSecond(4622470422L)))
            .signWith(
                SignatureAlgorithm.HS256,
                "Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)
            )
            .compact();
        return new ResponseEntity<>(new AuthResponse(jws),HttpStatus.OK);
    }
}

