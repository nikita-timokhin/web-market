package webmarket.controllers;

import org.springframework.http.HttpStatus;
import webmarket.models.Type;
import org.springframework.web.bind.annotation.*;
import webmarket.repository.TypeRepository;

@RestController
@RequestMapping("type")
public class TypeController {
    private final TypeRepository typeRepository;

    public TypeController(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    @GetMapping("/list")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Type> allPosts() {
        return this.typeRepository.findAll();
    }

    @PostMapping("/add")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void add(@RequestBody Type type) {
        this.typeRepository.save(type);
    }

    @PostMapping("/del")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void delete() {
        this.typeRepository.deleteAll();
    }
}
