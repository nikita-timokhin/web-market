package webmarket.controllers;

import org.springframework.http.HttpStatus;
import webmarket.models.User;
import org.springframework.web.bind.annotation.*;
import webmarket.repository.UserRepository;
import webmarket.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {

    private final UserRepository userRepository;
    private final UserService userService;

    public UserController(UserRepository user, UserService userService) {
        this.userRepository = user;
        this.userService = userService;
    }


    @GetMapping("/list")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public Iterable<User> res() {
        return userRepository.findAll();
    }

    @PostMapping("/add")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void add(@RequestBody User user) {
        this.userRepository.save(user);
    }

    @PostMapping("/del")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void delete() {
        this.userRepository.deleteAll();
    }

    @PostMapping("/alter")
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public void increase(@RequestBody  User user){
        userService.increaseAmount(user);
    }

}