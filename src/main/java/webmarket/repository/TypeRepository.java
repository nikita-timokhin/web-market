package webmarket.repository;

import webmarket.models.Type;
import org.springframework.data.repository.CrudRepository;


public interface TypeRepository extends CrudRepository<Type, Integer> {
}
