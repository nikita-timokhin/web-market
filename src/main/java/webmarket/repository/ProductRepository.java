package webmarket.repository;

import webmarket.models.Authentication;
import webmarket.models.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    Optional<Product> getByName(String name);
}
