package webmarket.repository;

import webmarket.models.Authentication;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AuthenticationRepository extends CrudRepository<Authentication, Integer> {
    Optional<Authentication> getByLoginAndPass(String login, int pass);
}
