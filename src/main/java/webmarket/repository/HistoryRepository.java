/*
 * Copyright
 */

package webmarket.repository;

import org.aspectj.weaver.ast.Or;
import org.springframework.data.repository.CrudRepository;
import webmarket.models.OrderHistory;

import java.util.List;

public interface HistoryRepository extends CrudRepository<OrderHistory, Integer> {
    List<OrderHistory> findAllByUserId(int id);
    List<OrderHistory> findAllByName(String name);
}
