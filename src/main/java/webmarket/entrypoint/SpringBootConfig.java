/*
 * Copyright
 */

package webmarket.entrypoint;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring boot config class.
 *
 * @since 0.0.1
 */
@SpringBootApplication(scanBasePackages = "webmarket")
@EnableJpaRepositories(basePackages = "webmarket")
@EntityScan(basePackages = "webmarket")
public class SpringBootConfig {
}
